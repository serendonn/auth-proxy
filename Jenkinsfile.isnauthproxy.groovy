@Library('jenkins-utilities@master') _

def not = new dstvo.jenkinsutilities.Notify()

node('docker && isn') {
    not.notify('CV-AutomatedTestnotifications@multichoice.co.za') {
        stage('Retrieving latest source') {
            checkout scm
        }

        stage('Running tests...') {
            new dstvo.jenkinsutilities.Docker().runMavenTests("mvn -f pom.xml test -DtestSuite=AuthProxy")
        }
    }
}
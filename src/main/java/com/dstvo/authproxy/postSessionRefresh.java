package com.dstvo.authproxy;

import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import com.dstvo.helpers.APIRequestUtils;



public class postSessionRefresh extends APIRequestUtils{



    @Test
    public void postSession(){

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", username);
        requestParams.put("password", password);
        requestParams.put("clientIp", "127.0.0.1");
        requestParams.put("ttlsec", "60");

        Response response =
                given().
                        header("Authorization",authorization).
                        header("Accept","application/json").
                        contentType("application/json").
                        body(requestParams.toJSONString()).
                when().
                        post("/session").
                then().
                        log().everything().
                        statusCode(200).
                        body("sessionId", not(equalTo(null))).
                        extract().response();

        cookie = response.headers().getValues("Set-Cookie");
        System.out.println(cookie);
    }


    @Test(dependsOnMethods = "postSession")
    public void refreshSession(){
        JSONObject requestParams = new JSONObject();
        requestParams.put("password", password);
        requestParams.put("ttlsec", "1");

        System.out.println(cookie);

        Response response =
                given().
                        header("Authorization",authorization).
                        header("cookie",cookie.toString()).
                        header("Accept","application/json").
                        contentType("application/json").
                        body(requestParams.toJSONString()).
                when().
                        post("session/refresh").
                then().
                        log().everything().
                        statusCode(200).
                        body("sessionId", not(equalTo(null))).
                        body("connectId", not(equalTo(null))).
                        body("status",equalTo("ok")).
                        extract().response();
    }


    @Test(dependsOnMethods = "refreshSession")
    public void sessionRefreshExpired() throws InterruptedException {

        Thread.sleep(1000);
        System.out.println(cookie);
        Response response =
                given().
                        header("Authorization",authorization).
                        header("cookie",cookie.toString()).
                when().
                        get("/session").
                then().
                        log().everything().
                        statusCode(200).
                        body("sessionId", not(equalTo(null))).
                        body("connectId", not(equalTo(null))).
                        body("status",equalTo("expired")).
                        extract().response();
    }
}
